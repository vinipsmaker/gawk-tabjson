# DO NOT CALL THIS SCRIPT DIRECTLY.
# Run `meson test` on the build dir.

TEST="$1"

if "$GAWK_BIN" -v TEST="$TEST" -e 'BEGIN { exit TEST ~ /fail_[^/]+$/ }'; then
    set -e
    set -o pipefail

    # mmap implementation
    "$GAWK_BIN" -l tabjson -f "$TEST.awk" -- "$TEST.in" |
        "$DIFF_BIN" -u "$TEST.out" -

    # read-loop implementation
    "$CAT_BIN" "$TEST.in" | "$GAWK_BIN" -l tabjson -f "$TEST.awk" |
        "$DIFF_BIN" -u "$TEST.out" -
else
    # mmap implementation
    "$GAWK_BIN" -l tabjson -f "$TEST.awk" -- "$TEST.in"
    if [ $? -eq 0 ]; then
        exit 1
    else
        # read-loop implementation
        "$CAT_BIN" "$TEST.in" | "$GAWK_BIN" -l tabjson -f "$TEST.awk"
        if [ $? -eq 0 ]; then
            exit 1
        else
            exit 0
        fi
    fi
fi
