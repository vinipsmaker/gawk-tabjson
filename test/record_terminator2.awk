@include "ord"

BEGIN {
    JPAT[1] = "/1"
    JUNWIND = 1
}
{
    printf "length=%i", length(RT)
    for (i = 1 ; i <= length(RT) ; ++i) {
        printf " 0x%02X", ord(substr(RT, i, 1))
    }
    print ""
}
