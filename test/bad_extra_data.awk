# This test will break once customizable error handling lands.
BEGIN {
    JPAT[1] = "/foo"
}
{ print "record:", $0, "foo:", J[1] }
