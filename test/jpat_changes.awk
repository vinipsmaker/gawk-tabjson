BEGIN {
    JPAT[1] = "/foo"
    JPAT[2] = "/bar"
}
J[1] == 1 {
    print J[2]
    JPAT[1] = "/foo2"
    JPAT[2] = "/bar2"
}
J[1] == 2 {
    print J[2]
    JPAT[1] = "/foo3"
    JPAT[2] = "/bar3"
}
J[1] == 3 { print J[2] }
