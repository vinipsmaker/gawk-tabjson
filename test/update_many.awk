BEGIN {
    JPAT[1] = "/foo"
    JPAT[2] = "/bar"
}
{ J[1] += 1 }
NR == 1 { J[2] = 7 }
NR == 2 { J[2] = 8 }
NR == 3 {
    print typeof(J[2])
    # NOTE: a future release of tabjson should gain the ability to insert new
    # fields (#18). By then, this test will have to be updated.
    J[2] = 9
}
{ print NR, tabjson::sprint() }
