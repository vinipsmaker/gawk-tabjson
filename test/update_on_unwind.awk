BEGIN {
    JPAT[1] = "/foo"
    JUNWIND = 1
}
{
    total += J[1]
    J[1] = NR
    print tabjson::sprint()
}
END {
    print total
}
