BEGIN {
    JPAT[1] = "/foo"
}
J[1] == 0 { J[1] = 1 }
J[1] == 10 { J[1] = 20 }
J[1] == 11 { J[1] = "some string" }
J[1] == 12 { delete J[1] }
{ print NR, tabjson::sprint() }
