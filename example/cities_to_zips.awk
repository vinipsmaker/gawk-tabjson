# Dataset: http://media.mongodb.org/zips.json
# In MongoDB:
# db.zips.aggregate([{"$group":{"_id":"$city", "postal_codes":{"$addtoSet":"$_id"}}}])

BEGIN {
    JPAT[1] = "/city"
    JPAT[2] = "/_id"
}
{ postal_codes[J[1]][J[2]] = 1 }
END {
    for (city in postal_codes) {
        print city ":"
        for (code in postal_codes[city]) {
            print "", code
        }
    }
}
