# Dataset: http://media.mongodb.org/zips.json
# In MongoDB:
# db.zips.aggregate([{"$match":{"pop":{"$gt":100000}}}])

BEGIN { JPAT[1] = "/pop" }
J[1] > 100000
