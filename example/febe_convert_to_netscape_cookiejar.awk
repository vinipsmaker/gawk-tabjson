# This script converts FEBE's cookie backups[1] to Netscape HTTP cookie files[2]
# as accepted by curl, wget, youtube-dl, and numerous other tools.
#
# [1] http://softwarebychuck.com/febe/febe.html
# [2] https://curl.se/docs/http-cookies.html

@load "tabjson"

BEGIN {
    JPAT[DOMAIN             = 1] = "/host"
    JPAT[INCLUDE_SUBDOMAINS = 2] = "/isDomain"
    JPAT[PATH               = 3] = "/path"
    JPAT[HTTPS_ONLY         = 4] = "/isSecure"
    JPAT[EXPIRES            = 5] = "/expires"
    JPAT[NAME               = 6] = "/name"
    JPAT[VALUE              = 7] = "/value"

    OFS="\t"

    print "# Netscape HTTP Cookie File"
    print "# http://softwarebychuck.com/febe/febe.html"
    print "# https://gitlab.com/vinipsmaker/gawk-tabjson/-/blob/master/example/febe_convert_to_netscape_cookiejar.awk"
}

# If you want to skip some entries from the output, add rules below this
# comment. For instance, to skip entries from "google.com", add the following
# rule:
#
# J[DOMAIN] ~ /google\.com/ { next }

{
    print J[DOMAIN],
        J[INCLUDE_SUBDOMAINS] ? "TRUE" : "FALSE",
        J[PATH],
        J[HTTPS_ONLY] ? "TRUE" : "FALSE",
        J[EXPIRES],
        J[NAME],
        J[VALUE]
}
