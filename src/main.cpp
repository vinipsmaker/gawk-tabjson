/* Copyright (c) 2020, 2021 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

// 4MiB was the initial per-document size limit in MongoDB
#define INITIAL_BUFFER_SIZE 4 * 1024 * 1024

extern "C" {
#include <unistd.h>
#include <sys/mman.h>
}

#include <string_view>
#include <charconv>
#include <cassert>
#include <string>

#include <boost/hana.hpp>

#include <trial/protocol/json/partial/skip.hpp>
#include <trial/protocol/buffer/string.hpp>
#include <trial/protocol/json/reader.hpp>
#include <trial/protocol/json/writer.hpp>

#include <tabjson/jpat.hpp>

// Must be included before gawkapi {{{
#include <cstdio>
#include <cerrno>
#include <cstddef>
#include <cstring>
#include <cstdlib>

extern "C" {
#include <sys/stat.h>
}

using std::FILE;
using std::memcpy;
using std::memset;
using std::size_t;
using std::exit;
// }}}

#include <gawkapi.h>

namespace hana = boost::hana;
namespace json = trial::protocol::json;

static_assert(GAWK_API_MAJOR_VERSION == 3 && GAWK_API_MINOR_VERSION >= 0,
              "This plug-in has been developed against documentation for gawk "
              "5.1.0. Please update your dist.");

// used in gawk macros {{{
const gawk_api_t *api;
awk_ext_id_t ext_id;
static const char *ext_version = "tabjson extension: version 0.1";
// }}}

int plugin_is_GPL_compatible;

static awk_bool_t can_take_file(const awk_input_buf_t* iobuf);
static awk_bool_t take_control_of(awk_input_buf_t* iobuf);

awk_array_t var_jpat;
awk_scalar_t var_junwind;
std::vector<Slice> slices;
static awk_array_t var_j;
static awk_scalar_t var_juindex;
static awk_scalar_t var_julength;
static char* cursor;
static awk_input_parser_t parser = {
    /*name=*/"tabjson",
    can_take_file,
    take_control_of,
    /*next=*/NULL //< gawk internal use
};
static const awk_fieldwidth_info_t zero_fields = {
    /*use_chars=*/awk_false,
    /*nf=*/0,
    /*fields=*/{}
};
static std::string buffer;
static std::string::size_type buffer_used;
static std::string_view mmap_buffer;
static std::string::size_type record_size;
static std::string::size_type record_rt_size;

int unwind_idx; //< 1-indexed ; 0 is root ; -1 is unwind unset
static std::vector<awk_value_t> unwinded_values;
static bool unwind_in_progress;
static size_t nunwinded;

constexpr auto max_digits = hana::second(hana::while_(
    [](auto s) { return hana::first(s) > hana::size_c<0>; },
    hana::make_pair(
        /*i=*/hana::size_c<std::numeric_limits<size_t>::max()>,
        /*max_digits=*/hana::size_c<0>),
    [](auto s) {
        return hana::make_pair(
            hana::first(s) / hana::size_c<10>,
            hana::second(s) + hana::size_c<1>);
    }
));

void fill_j(json::reader& reader, Tree& node)
{
    auto consume_value = [&](size_t idx) {
        Slice slice;
        slice.rel_off = reader.literal().data() - cursor;
        slice.jidx = idx;
        slice.bool_src = false;

        switch (reader.symbol()) {
        case json::token::symbol::end:
        case json::token::symbol::error:
        case json::token::symbol::end_array:
        case json::token::symbol::end_object:
        case json::token::symbol::key:
            assert(false);
        case json::token::symbol::boolean: {
            awk_value_t idx_as_val;
            awk_value_t value;
            set_array_element(
                var_j,
                make_number(idx, &idx_as_val),
                make_number(reader.value<bool>() ? 1 : 0, &value));

            slice.bool_src = true;
        }
            if (false)
        case json::token::symbol::integer:
        case json::token::symbol::real: {
            awk_value_t idx_as_val;
            awk_value_t value;
            set_array_element(
                var_j,
                make_number(idx, &idx_as_val),
                make_number(reader.value<double>(), &value));
        }
            if (false)
        case json::token::symbol::string: {
            awk_value_t idx_as_val;
            awk_value_t value_as_val;
            auto value = reader.value<std::string>();
            char* mem = (char*)gawk_malloc(value.size() + 1);
            memcpy(mem, value.c_str(), value.size() + 1);
            set_array_element(
                var_j,
                make_number(idx, &idx_as_val),
                make_malloced_string(mem, value.size(), &value_as_val));
        }
            [[fallthrough]];
        case json::token::symbol::null:
            slice.size = reader.literal().size();
            if (!reader.next()) throw json::error{reader.error()};
            break;
        case json::token::symbol::begin_array:
            if ((int)idx == unwind_idx) {
                const char* head = reader.literal().data();
                if (!reader.next()) throw json::error{reader.error()};
                for (;;) {
                    if (reader.symbol() == json::token::symbol::end_array)
                        break;

                    awk_value_t value;

                    switch (reader.symbol()) {
                    case json::token::symbol::end:
                    case json::token::symbol::error:
                    case json::token::symbol::end_object:
                    case json::token::symbol::end_array:
                    case json::token::symbol::key:
                        assert(false);
                    case json::token::symbol::boolean:
                        make_number(reader.value<bool>() ? 1 : 0, &value);
                        break;
                    case json::token::symbol::integer:
                    case json::token::symbol::real:
                        make_number(reader.value<double>(), &value);
                        break;
                    case json::token::symbol::string: {
                        auto data = reader.value<std::string>();
                        char* mem = (char*)gawk_malloc(data.size() + 1);
                        memcpy(mem, data.c_str(), data.size() + 1);
                        make_malloced_string(mem, data.size(), &value);
                    }
                        break;
                    case json::token::symbol::null:
                        value.val_type = AWK_UNDEFINED;
                        break;
                    case json::token::symbol::begin_array:
                    case json::token::symbol::begin_object:
                        value.val_type = AWK_UNDEFINED;
                        unwinded_values.push_back(value);
                        json::partial::skip(reader);
                        continue;
                    }
                    unwinded_values.push_back(value);
                    if (!reader.next()) throw json::error{reader.error()};
                }
                assert(reader.symbol() == json::token::symbol::end_array);
                const char* tail = reader.literal().end();
                if (!reader.next()) throw json::error{reader.error()};
                slice.size = std::distance(head, tail);
                {
                    awk_value_t val;
                    auto ok = sym_update_scalar(
                        var_julength,
                        make_number(unwinded_values.size(), &val));
                    assert(ok); (void)ok;
                }
                unwind_in_progress = true;
                nunwinded = 0;
                break;
            }
            [[fallthrough]];
        case json::token::symbol::begin_object:
            slice.size = json::partial::skip(reader).size();
        }
        cursor += slice.rel_off + slice.size;
        slices.push_back(slice);
    };

    switch (reader.symbol()) {
    case json::token::symbol::begin_object: {
        if (!reader.next()) throw json::error{reader.error()};

        std::string current_key;
        for (;;) {
            if (reader.symbol() == json::token::symbol::end_object) {
                reader.next();
                break;
            }

            // Key
            assert(reader.symbol() == json::token::symbol::key);
            current_key.clear();
            auto ec = reader.string(current_key);
            assert(!ec); (void)ec;

            if (!reader.next()) throw json::error{reader.error()};

            // Value
            auto it = node.find(current_key);
            if (it == node.end()) {
                json::partial::skip(reader);
                continue;
            }
            std::visit(
                hana::overload(
                    [&](Tree& tree) {
                        switch (reader.symbol()) {
                        case json::token::symbol::begin_array:
                        case json::token::symbol::begin_object:
                            fill_j(reader, tree);
                            break;
                        default:
                            json::partial::skip(reader);
                        }
                    },
                    consume_value),
                it->second);
        }

        if (reader.symbol() == json::token::symbol::error)
            throw json::error{reader.error()};
    }
        break;
    case json::token::symbol::begin_array: {
        if (!reader.next()) throw json::error{reader.error()};

        std::array<char, max_digits> current_key;
        for (size_t i = 1 ;; ++i) {
            if (reader.symbol() == json::token::symbol::end_array) {
                reader.next();
                break;
            }

            auto s_size = std::to_chars(current_key.data(),
                                        current_key.data() + current_key.size(),
                                        i).ptr - current_key.data();

            auto it = node.find(std::string_view(current_key.data(), s_size));
            if (it == node.end()) {
                json::partial::skip(reader);
                continue;
            }
            std::visit(
                hana::overload(
                    [&](Tree& tree) {
                        switch (reader.symbol()) {
                        case json::token::symbol::begin_array:
                        case json::token::symbol::begin_object:
                            fill_j(reader, tree);
                            break;
                        default:
                            json::partial::skip(reader);
                        }
                    },
                    consume_value),
                it->second);
        }

        if (reader.symbol() == json::token::symbol::error)
            throw json::error{reader.error()};
    }
        break;
    default:
        assert(false);
    }
}

static int get_record(char** out, struct awk_input* iobuf, int* errcode,
                      char** rt_start, size_t* rt_len,
                      const awk_fieldwidth_info_t** field_width)
{
    assert(out != nullptr);
    assert(errcode != nullptr);
    assert(rt_start != nullptr);
    assert(rt_len != nullptr);

    if (unwind_in_progress && nunwinded < unwinded_values.size()) {
        if (mmap_buffer.size() > 0) {
            *out = const_cast<char*>(mmap_buffer.data()) + buffer_used;
        } else {
            *out = buffer.data();
        }
        *rt_start = *out + record_size - record_rt_size;
        *rt_len = record_rt_size;

        awk_value_t key;
        if (unwinded_values[nunwinded].val_type == AWK_UNDEFINED) {
            del_array_element(var_j, make_number(unwind_idx, &key));
        } else {
            set_array_element(
                var_j,
                make_number(unwind_idx, &key),
                &unwinded_values[nunwinded]);
        }

        {
            awk_value_t val;
            auto ok = sym_update_scalar(var_juindex,
                                        make_number(++nunwinded, &val));
            assert(ok); (void)ok;
        }

        return record_size - *rt_len;
    }
    unwind_in_progress = false;

    if (mmap_buffer.size() > 0) {
        buffer_used += record_size;
    } else {
        buffer.erase(0, record_size);
        buffer.resize(buffer.size() + record_size);
        buffer_used -= record_size;
    }
    record_size = 0;

    read_jpat();
    clear_array(var_j);
    {
        awk_value_t val;
        auto ok = sym_update_scalar(var_juindex, make_number(0, &val));
        assert(ok); (void)ok;
    }
    slices.clear();
    unwinded_values.clear();

    std::string_view line;

    if (mmap_buffer.size() > 0) {
        std::string_view buffer_view = mmap_buffer;
        buffer_view.remove_prefix(buffer_used);
        if (buffer_view.size() == 0)
            return EOF;

        auto lf = buffer_view.find('\n');
        if (lf != std::string_view::npos)
            line = buffer_view.substr(0, lf + 1);
        else
            line = buffer_view;

        cursor = const_cast<char*>(buffer_view.data());
    } else {
        for (;;) {
            std::string_view buffer_view(buffer.data(), buffer_used);
            auto lf = buffer_view.find('\n');
            if (lf != std::string_view::npos) {
                line = buffer_view.substr(0, lf + 1);
                break;
            }

            if (buffer.size() == buffer_used)
                buffer.resize(buffer.size() * 2);

            ssize_t nread = read(iobuf->fd,
                                 const_cast<char*>(buffer.data()) + buffer_used,
                                 buffer.size() - buffer_used);
            if (nread == -1) {
                *errcode = errno;
                return EOF;
            }
            if (nread == 0) {
                if (buffer_used == 0)
                    return EOF;

                line = std::string_view{buffer.data(), buffer_used};
                break;
            }
            buffer_used += nread;
        }

        cursor = buffer.data();
    }

    record_size = line.size();

    if (field_width != nullptr)
        *field_width = &zero_fields;
    record_rt_size = *rt_len = 0;

    {
        auto idx = line.find_last_not_of("\r\n");
        if (idx != std::string_view::npos) {
            *rt_start = cursor + idx + 1;
            record_rt_size = *rt_len = line.size() - idx - 1;
            if (*rt_len == 0)
                *rt_start = nullptr;
        }
    }

    json::reader reader{line};
    try {
        switch (reader.symbol()) {
        case json::token::symbol::error:
            throw json::error{reader.error()};
        case json::token::symbol::begin_object:
        case json::token::symbol::begin_array:
            fill_j(reader, tree);
            break;
        case json::token::symbol::boolean: {
            awk_value_t key;
            awk_value_t value;
            set_array_element(
                var_j,
                make_number(0, &key),
                make_number(reader.value<bool>() ? 1 : 0, &value));
        }
            if (false)
        case json::token::symbol::integer:
        case json::token::symbol::real: {
            awk_value_t key;
            awk_value_t value;
            set_array_element(
                var_j,
                make_number(0, &key),
                make_number(reader.value<double>(), &value));
        }
            if (false)
        case json::token::symbol::string: {
            awk_value_t key;
            awk_value_t value_as_val;
            auto value = reader.value<std::string>();
            char* mem = (char*)gawk_malloc(value.size() + 1);
            memcpy(mem, value.c_str(), value.size() + 1);
            set_array_element(
                var_j,
                make_number(0, &key),
                make_malloced_string(mem, value.size(), &value_as_val));
        }
            [[fallthrough]];
        case json::token::symbol::null:
            reader.next();
            break;
        case json::token::symbol::end:
            // RT isn't set on absent records
            break;
        default:
            assert(false);
        }
        if (reader.symbol() != json::token::symbol::end)
            throw json::error{reader.error()};

        if (mmap_buffer.size() > 0)
            *out = const_cast<char*>(mmap_buffer.data()) + buffer_used;
        else
            *out = buffer.data();
    } catch (const std::exception& e) {
        for (auto& v: unwinded_values) {
            if (v.val_type == AWK_STRING)
                gawk_free(v.str_value.str);
        }
        unwinded_values.clear();

        unwind_in_progress = false;
        *out = nullptr;
        *rt_start = nullptr;
        record_rt_size = *rt_len = 0;
        if (mmap_buffer.size() > 0) {
            buffer_used += record_size;
        } else {
            buffer.erase(0, record_size);
            buffer.resize(buffer.size() + record_size);
            buffer_used -= record_size;
        }
        record_size = 0;
        warning(ext_id, "Error while processing record: %s", e.what());
    }
    if (unwind_in_progress) {
        if (unwinded_values.size() == 0) {
            // recursion is not the best strategy here, but right now I'm short
            // on time to write a more robust solution
            return get_record(out, iobuf, errcode, rt_start, rt_len,
                              field_width);
        }

        awk_value_t key;
        if (unwinded_values[0].val_type == AWK_UNDEFINED) {
            del_array_element(var_j, make_number(unwind_idx, &key));
        } else {
            set_array_element(
                var_j,
                make_number(unwind_idx, &key),
                &unwinded_values[0]);
        }

        {
            awk_value_t val;
            auto ok = sym_update_scalar(var_juindex, make_number(1, &val));
            assert(ok); (void)ok;
        }

        nunwinded = 1;
    }

    return record_size - *rt_len;
}

static awk_bool_t can_take_file(const awk_input_buf_t* iobuf)
{
    // TODO: maybe we should also check whether len(JPAT) != 0
    return iobuf->fd != INVALID_HANDLE ? awk_true : awk_false;
}

static awk_bool_t take_control_of(awk_input_buf_t* iobuf)
{
    assert(iobuf->fd != INVALID_HANDLE);
    iobuf->get_record = get_record;
    buffer_used = 0;
    if (mmap_buffer.size() > 0) {
        munmap(const_cast<char*>(mmap_buffer.data()), mmap_buffer.size());
        mmap_buffer = std::string_view{};
    }
    {
        size_t len = iobuf->sbuf.st_size;
        if (len == 0)
            return awk_true;

        auto pa = mmap(/*addr=*/0, len, PROT_READ, MAP_SHARED, iobuf->fd,
                       /*offset=*/0);
        if (pa == MAP_FAILED)
            return awk_true;

        mmap_buffer = std::string_view{reinterpret_cast<char*>(pa), len};
    }
    return awk_true;
}

static awk_value_t* do_sprint(int /*num_actual_args*/, awk_value_t* result,
                              awk_ext_func_t* /*finfo*/)
{
    size_t output_size = 0;
    for (auto& slice: slices) {
        output_size += slice.rel_off;
        slice.buffer.clear();

        awk_value_t idx_as_val; make_number(slice.jidx, &idx_as_val);
        awk_value_t value;

        if (!get_array_element(var_j, &idx_as_val, AWK_UNDEFINED, &value)) {
            slice.buffer = "null";
            output_size += slice.buffer.size();
            continue;
        }
        switch (value.val_type) {
        case AWK_UNDEFINED:
        case AWK_ARRAY:
            slice.buffer = "null";
            break;
        case AWK_NUMBER:
            if (value.num_value == 1.0 && slice.bool_src) {
                slice.buffer = "true";
            } else if (value.num_value == 0.0 && slice.bool_src) {
                slice.buffer = "false";
            } else {
                // TODO: respect AWK's OFMT
                json::writer writer{slice.buffer};
                writer.value(value.num_value);
            }
            break;
        case AWK_STRING:
        case AWK_REGEX:
        case AWK_STRNUM: {
            json::writer writer{slice.buffer};
            writer.value(json::writer::view_type{
                value.str_value.str, value.str_value.len});
            break;
        }
        case AWK_SCALAR:
        case AWK_VALUE_COOKIE:
            assert(false);
        }
        output_size += slice.buffer.size();
    }
    auto in_it = []() -> const char* {
        if (mmap_buffer.size() > 0)
            return mmap_buffer.data() + buffer_used;
        else
            return buffer.data();
    }();
    auto buffer_end = in_it + record_size - record_rt_size;
    output_size += buffer_end - cursor;
    char*const mem = (char*)gawk_malloc(output_size + 1);
    auto out_it = mem;
    for (auto& slice: slices) {
        memcpy(out_it, in_it, slice.rel_off);
        out_it += slice.rel_off;
        in_it += slice.rel_off + slice.size;
        memcpy(out_it, slice.buffer.data(), slice.buffer.size());
        out_it += slice.buffer.size();
    }
    memcpy(out_it, cursor, buffer_end - cursor);
    out_it += buffer_end - cursor;
    *out_it = '\0';
    return make_malloced_string(mem, output_size, result);
}

static awk_bool_t init_func()
{
    buffer.resize(INITIAL_BUFFER_SIZE);
    {
        var_jpat = create_array();
        awk_value_t val;
        val.val_type = AWK_ARRAY;
        val.array_cookie = var_jpat;
        if (!sym_update("JPAT", &val)) {
            fatal(ext_id, "Failed to create `JPAT` array");
            return awk_false;
        }
        var_jpat = val.array_cookie;
    }
    {
        awk_value_t value;
        if (!sym_update("JUNWIND", make_number(-1, &value))) {
            fatal(ext_id, "Failed to create `JUNWIND` scalar");
            return awk_false;
        }
        awk_bool_t ok = sym_lookup("JUNWIND", AWK_SCALAR, &value);
        assert(ok == awk_true);
        (void)ok;
        var_junwind = value.scalar_cookie;
    }
    {
        awk_value_t value;
        if (!sym_update("JUINDEX", make_number(0, &value))) {
            fatal(ext_id, "Failed to create `JUINDEX` scalar");
            return awk_false;
        }
        awk_bool_t ok = sym_lookup("JUINDEX", AWK_SCALAR, &value);
        assert(ok == awk_true);
        (void)ok;
        var_juindex = value.scalar_cookie;
    }
    {
        awk_value_t value;
        if (!sym_update("JULENGTH", make_number(0, &value))) {
            fatal(ext_id, "Failed to create `JULENGTH` scalar");
            return awk_false;
        }
        awk_bool_t ok = sym_lookup("JULENGTH", AWK_SCALAR, &value);
        assert(ok == awk_true);
        (void)ok;
        var_julength = value.scalar_cookie;
    }
    {
        var_j = create_array();
        awk_value_t val;
        val.val_type = AWK_ARRAY;
        val.array_cookie = var_j;
        if (!sym_update("J", &val)) {
            fatal(ext_id, "Failed to create `J` array");
            return awk_false;
        }
        var_j = val.array_cookie;
    }

    register_input_parser(&parser);

    return awk_true;
}

static awk_ext_func_t func_table[] = {
    {
        "sprint", do_sprint,
        /*max_expected_args=*/0,
        /*min_required_args=*/0,
        /*suppress_lint=*/awk_false,
        /*data=*/NULL
    },
    { NULL, NULL, 0, 0, awk_false, NULL }
};

// Uses `api`, `ext_id`, `init_func` and `ext_version`
dl_load_func(func_table, tabjson, /*namespace=*/"tabjson")
