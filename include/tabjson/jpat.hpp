/* Copyright (c) 2020 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#pragma once

#include <string_view>
#include <variant>
#include <string>
#include <map>

struct TreeComp
{
    using is_transparent = void;

    bool operator()(const std::string& lhs, const std::string& rhs) const
    {
        return lhs < rhs;
    }

    bool operator()(const std::string_view& lhs, const std::string& rhs) const
    {
        return lhs < rhs;
    }

    bool operator()(const std::string& lhs, const std::string_view& rhs) const
    {
        return lhs < rhs;
    }
};

// the leaf node stores the target J index
struct Tree: std::map<std::string, std::variant<Tree, size_t>, TreeComp>
{};

struct Slice
{
    // Position counting from last match. Another equally valid interpretation
    // for this value is the **size** of the preceding slice that only needs to
    // be passed through and needs no special treatment (i.e. it's a preceding
    // slice that won't be replaced by the value of any element from the J
    // array).
    size_t rel_off;

    size_t size;
    size_t jidx;
    bool bool_src;

    // Invalid most of the time. It may be filled from anywhere to hold
    // temporaries.
    std::string buffer;
};

extern Tree tree;

void read_jpat();
